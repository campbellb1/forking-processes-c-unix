/*
 * Message.h
 * Programmer: Brandon Campbell
 * Date: 2/13/2018
 * Purpose: This struct is used to house information regarding a command file
 */

#ifndef UNTITLED_MESSAGE_H
#define UNTITLED_MESSAGE_H

/*
 * This struct will hold a Commands message.
 */
struct Message
{
    int id;         //unique identification numbered
    char command;   //Which command
    char key[16];   //Key value for the note
    char payload[128];//The short note
};

#endif //UNTITLED_MESSAGE_H

