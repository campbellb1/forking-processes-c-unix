
/*  Programmer: Brandon Campbell
 *  Date:       1/25/2018 - 3/15/2018
 *  Project:    Homework 3
 *  Purpose:    Demonstrates forking multiple processes from a single parent.
 *              Performs Logging, Creating Files, Searching for Files, and counting
 *              the number of key files in a folder.
 */

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Unix library
#include <unistd.h>
#include <dirent.h>


#include "SetupData.h"
#include "Log.h"
#include "Message.h"
#include <wait.h>

// Shortcut for the std namespace
using namespace std;

// String length, plus one for \0
#define STRLEN 32

int openCommandFile(string cmdFile);
int readCmdFile(Message &msg);
void createLogger(int loggerPipe[], Message msg);
void createPut(int putPipe[], Message msg, int returnPipe[]);
void createSearch(int searchPipe[], Message msg, int returnPipe[]);
void createNumber(int numberPipe[], Message msg, int returnPipe[]);
int openPipes(int putPipe[], int loggerPipe[], int setPipe[], int searchPipe[], int returnPipe[] );
void closePipes(int putPipe[], int loggerPipe[], int numberPipe[], int searchPipe[], int returnPipe[] );
string duplicateMsg = "Duplicate";
string okayMsg = "Okay";
string notFoundString = "Not Found";
string errorMessage = "Error!";



fstream cmdStream;
int msgCounter;
Log log;

//
// main( )
//
//    Shows the use of argc and argv
//      and retrieving argv from getopt( )
//
int main( int argc, char** argv )
{
    //SetupData setupData;
    string dataString;
    string strPathToSetupFile;
    string strNameOfSetupFile;
    char* tempMsgString;
    int setupDataIsPresent;
    //The following serve as the communication pipes for the processes to pass messages.
    int putPipe[2];
    int numberPipe[2];
    int searchPipe[2];
    int returnPipe[2];
    int loggerPipe[2];
    Message msg;    //a message struct that will hold messages as they are read in.


    enum childTypes {NUMBER = 0, SEARCH = 1, PUT = 2, LOGGER = 3  }; //the types of child processes.

    //holds the path and name of the setupfile.
    char pathToSetupFile[STRLEN] = "", nameOfSetupFile[STRLEN] = "";
    // For return value of getopt( )
    char argFlag;


    // Use getopt( ) to loop through -p (path to find setupfile), -s (setupfilename -- optional) flags
    while ( (argFlag = getopt(argc, argv, "p:s:")) != -1 )
    {
        switch (argFlag)
        {
            case 'p': strncpy(pathToSetupFile, optarg, STRLEN-1);
                break;
            case 's': strncpy(nameOfSetupFile, optarg, STRLEN-1);
                break;
            default:
                break;
        }; // switch
    } // while

    //instantiate these strings to properly pass in strings to the overloaded constructor.
    strPathToSetupFile = pathToSetupFile;
    strNameOfSetupFile = nameOfSetupFile;
    //if we got a path in the arguments, put it in the SetupData
    if (!strlen(pathToSetupFile) > 0)
    {
        cout << "Usage: hw3 -p <pathname> [-s <setupfilename>]" << endl;
        exit(1);
    }

    //generate a SetupData from the passed in arguments.
    SetupData setupData(strPathToSetupFile, strNameOfSetupFile);

    //if we got a setupfile name, put it in the SetupData
    if(strlen(nameOfSetupFile) > 0)
    {
        setupData.setSetupfilename(nameOfSetupFile);
    }
    else    //otherwise put the Default setupfile name in: "setupfile"
    {
        setupData.setSetupfilename("setupfile");
    }

    setupDataIsPresent = setupData.open();  //see if we can open the setupdata file. . .
    if(setupDataIsPresent == 1) //check if we could open the file--if not, write an error message.
    {
        setupData.read();
        //store the SetupFile data into a temporary string for writing to the logfile.


        if(!setupData.getLogfilename().empty()) //if the setupdata contains some logfile name, replace the default.
        {
            log.setLogfileName(setupData.getLogfilename());
        }

        //Write the log records for the command file to the log file.

        //Begin opening of the commandfile.

        //attempt to open the command file and load in its data...
        if (openCommandFile(setupData.getCommandfilename().c_str()) == 1)
        {
            //try to open all of the pipes, if error, let user know.
            if(openPipes(putPipe, loggerPipe, numberPipe, searchPipe, returnPipe) == -1)
            {
                cout << "Error with logger pipe initialization!!";
                exit(1);
            }

            pid_t pid;  //will hold process id
            int type = 0; //the first child will be the LOGGER child.
            pid_t  pid_array[4];
            //create 4 child processes, the Logger, the Put, the Search, and the Number
            for(int i = 0; i < 4; i++)
            {
                if ((pid = fork()) < 0) //check for forking error
                {
                    perror ("Error on fork attempt!\n");
                    exit(2);
                }

                if (pid == 0) //if pid == 0, it's a child
                {//------CHILD CODE-------
                    size_t n = 0;
                    size_t p = getpid();
                    switch(type)    //"Activate" a child as a certain type as we loop through our fork
                    {
                        case LOGGER:
                            createLogger(loggerPipe, msg);//activate the Logger process
                            break;
                        case PUT:
                            createPut(putPipe, msg, returnPipe);//activate the Put(ter) process
                            break;
                        case SEARCH:
                            createSearch(searchPipe, msg, returnPipe);//activate the Search process
                            break;
                        case NUMBER:
                            createNumber(numberPipe, msg, returnPipe);//activate the Number process
                            break;
                        default:
                            break;
                    }
                    exit(0);
                }
                else //-------------PARENT CODE --------
                {
                    bool notQuit = true;    //this keeps track of whether or not we've read a 'Q'
                    char incomingMsg[80];   //give at least 80 bytes for incoming messages
                    type++;                 //increment our Child "Type"
                    int status;
                    char setupDataToSend[200];

                    string stringSetupInfo1 = "pathname: " + setupData.getPathname() +
                            ",setupfilename: " + setupData.getSetupfilename() +
                            ",logfile: " + setupData.getLogfilename() +
                            ",commandfile: " + setupData.getCommandfilename() +
                            ",username: " + setupData.getUsername() + ",";

                    strcpy(setupDataToSend, stringSetupInfo1.c_str());

                    if(type == 4)           //if we've made all of our children, go ahead and start reading the cmd file.
                    {
                        write(loggerPipe[1], stringSetupInfo1.c_str(), 200);

                        while(cmdStream.peek() != -1 && notQuit) //check to see if we've reached the end of the file and we haven't gotten a 'Q' msg.
                        {
                            if (readCmdFile(msg) == 1) //if there's a command to be read, read it and store it in msg
                            {
                                write(loggerPipe[1], (char *) &msg, sizeof(Message)); //go ahead and write to the Logger

                                //depending on the 'command', send the message to the appropriate handler.
                                //Once the message has been written, (wait and) read the response from the child.
                                switch(msg.command)
                                {
                                    case 'P':   //if we read a 'P', send the message to the Put Child.
                                        write(putPipe[1], (char*)&msg, sizeof(Message));
                                        read(returnPipe[0], incomingMsg, sizeof(incomingMsg));
                                        cout << "Put child reported to server: " << incomingMsg << endl;
                                        break;
                                    case 'S':   //if we read an 'S', send the message to the Search Child.
                                        write(searchPipe[1], (char*)&msg, sizeof(Message));
                                        read(returnPipe[0], incomingMsg, sizeof(incomingMsg));
                                        cout << "Search child reported to server: " << incomingMsg << endl;
                                        break;
                                    case 'N':   //if we read an 'N', send the message to the Number Child.
                                        write(numberPipe[1], (char*)&msg, sizeof(Message));
                                        read(returnPipe[0], incomingMsg, sizeof(incomingMsg));
                                        cout << "Number child reported to server: " << incomingMsg << endl;
                                        break;
                                    case 'Q':   //If we read a 'Q', tell all the children to die.
                                        write(putPipe[1], (char*)&msg, sizeof(Message));
                                        write(searchPipe[1], (char*)&msg, sizeof(Message));
                                        write(numberPipe[1], (char*)&msg, sizeof(Message));
                                        notQuit = false;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        cmdStream.close();
                        while (pid = waitpid(-1, NULL, 0)) //wait for all children to die
                        {
                            if (errno == ECHILD) //if our wait encountered the "no child error"...
                            {
                                break;  //break out of the loop
                            }
                        }
                        closePipes(putPipe, loggerPipe, numberPipe, searchPipe, returnPipe); //close all the pipes--as we are done reading.
                        setupData.close();
                    }

                }

            }

        }
        else    //of not found/able to read, print an error msg. to console and to the logfile.
        {
            log.writeLogRecord("Could not find/read the command file: \"" + setupData.getCommandfilename());
            cout << "Could not find/read the command file: \"" + setupData.getCommandfilename() + "\"" << endl;
        }


    }
    else //if we were not able to read/find the setupdata file, write such in the logfile, tell user, and exit.
    {
        dataString = "Set up file " + setupData.getSetupfilename() + " does not exist!";
        log.open();

        log.writeLogRecord(dataString);

        log.close();
        cout << "Could not find/read the setupfile: \"" + setupData.getSetupfilename() + "\"" << endl;
        exit(1);
    }

} // main( )

/*
 * open()
 * Opens the directory that is listed in pathName, then reads the setupfile located there
 */
int openCommandFile(string cmdFilePath)
{
    int returnValue = 0;

    cmdStream.open(cmdFilePath, ios::in);
    if (!cmdStream)  //otherwise we couldn't find/open the file, return 0.
    {
        returnValue = 0;
    } else    //if the file opens, it must be present, return 1.
    {
        returnValue = 1;
    }

    return returnValue;
}

/*
* read()
* Reads the data within the setup file that is currently in the filestream
*/
int readCmdFile(Message &msg)
{
    string line;    //holds the temporary string that is received from the getline

    //first check to see if the setupdata is in the standard format: i.e. logfile: mylog, if so
    //parse with a normal cin style

    getline(cmdStream, line);

    if(!line.empty()) //check for an empty line--if line is occupied, read triplet's contents.
    {
        msg.id = ++msgCounter;
        msg.command = char(line[0]);

        getline(cmdStream, line);
        strcpy(msg.key, line.c_str());

        getline(cmdStream, line);
        strcpy(msg.payload, line.c_str());

        return 1; //return 1 for success.
    } else
        return 0; //return 0 for a blank line--no content read.
}


/*
 * closePipes(int[], int[], int[], int[], int[])
 * Purpose: Closes all pipes used in the program.
 */
void closePipes(int putPipe[], int loggerPipe[], int numberPipe[], int searchPipe[], int returnPipe[] )
{
    int returnInt = 0;  //This will tell us if the pipe opening failed or not.

    //Closes all the pipes
    close(putPipe[0]);
    close(putPipe[1]);
    close(loggerPipe[0]);
    close(loggerPipe[1]);
    close(numberPipe[0]);
    close(numberPipe[1]);
    close(searchPipe[0]);
    close(searchPipe[1]);
    close(returnPipe[0]);
    close(returnPipe[1]);
}


/*
 * createLogger(int[], Message)
 * Purpose: Handles the functionality of the Logger child. Logs each message that is read by the
 *          main program.
 */
void createLogger(int loggerPipe[], Message msg)
{
    size_t pointer = 0;
    log.open();
    char incomingMessage[32];
    string strIncomingMessage;
    string token;
    string delimiter = ",";


    read(loggerPipe[0], incomingMessage, 200);
    strIncomingMessage = (string)incomingMessage;
    while ((pointer = strIncomingMessage.find(delimiter)) != std::string::npos)
    {
        token = strIncomingMessage.substr(0, pointer);
        log.writeLogRecord(token);
        strIncomingMessage.erase(0, pointer + delimiter.length());
    }


    /* Read in a string from the pipe */
    while(msg.command != 'Q')
    {
        close(loggerPipe[1]);
        read(loggerPipe[0], (char*)&msg, sizeof(Message));
        cout << "Log server received message #" << msg.id << ", key = " << msg.key << ", payload = " << msg.payload << endl;
        if(msg.command == 'Q')
        {
            log.close();
            exit(0);
        }
        log.writeLogRecord("Message ID: " + to_string(msg.id) + "\n");
        log.writeLogRecord("Command: " + string(1, msg.command) + "\n");
        log.writeLogRecord("Key: " + string(msg.key) + "\n");
        log.writeLogRecord("Payload: " + string(msg.payload) + "\n");
    }

}

/*
 * createNumber(int[], Message, int[])
 * Performs the process for the Number Child: counts the number
 * of "key files" in the current directory.
 */
void createNumber(int numberPipe[], Message msg, int returnPipe[])
{
    while(msg.command != 'Q')
    {
        read(numberPipe[0], (char *) &msg, sizeof(Message));
        cout << "Number server received message #" << msg.id << ", key = " << msg.key << ", payload = " << msg.payload << endl;
        if (msg.command == 'Q')
            exit(0);

        int file_count = 0;
        DIR * dirp;
        struct dirent * entry;

        dirp = opendir("./"); /* There should be error handling after this */
        while ((entry = readdir(dirp)) != NULL)
        {
            file_count++;
        }
        closedir(dirp);

        file_count -= 5;

        write(returnPipe[1],(to_string(file_count)).c_str(), sizeof((to_string(file_count)).c_str()));
    }
}

/*
 * createPut(int[], Message, returnPipe[])
 * The process for the Put Child; Creates a file by the name of the key value in the Message
 * the child receives. The contents of the file are the payload.
 */
void createPut(int putPipe[], Message msg, int returnPipe[])
{
    while(msg.command != 'Q')   //so long as our command isn't 'Q', continue to read in from the pipe.
    {
        read(putPipe[0], (char*)&msg, sizeof(Message)); //read in the message.
        //print out the message received.
        cout << "Put server received message #" << msg.id << ", key = " << msg.key << ", payload = " << msg.payload << endl;
        if(msg.command == 'Q') //if the child receives a 'Q', exit (child dies)
            exit(0);

        if(access(msg.key, F_OK ) != -1) //check to see if a file already exists by the name of the message key
        {
            write(returnPipe[1], duplicateMsg.c_str(), sizeof(duplicateMsg)); //if so, report 'Duplicate'
        }
        else
        {
            fstream putStream; //create an fstream for writing to the keyfile.


            putStream.open(msg.key, ios::app); //open the file for writing.

            //Write the beginning of the logfile if possible.
            if(!putStream)
            {
                cout << "Can't open the " << msg.key << " file!" << endl;
                write(returnPipe[1], errorMessage.c_str(), sizeof(errorMessage)); //return success to the parent.
            }
            else
            {
                putStream << msg.payload << endl;
                putStream.close();  //close the fstream.
                write(returnPipe[1], okayMsg.c_str(), sizeof(okayMsg)); //return success to the parent.
            }
        }
    }

}


/*
 * createSearch(int[], Message, int[])
 * This method handles the functionality of the Search Child. Searches for a file by the name of the
 * passed in message. Returns the text found within the file--if found--if not found, tell the parent
 * the file was not found.
 */
void createSearch(int searchPipe[], Message msg, int returnPipe[])
{
    fstream searchStream;
    string payload;
    bool fileOpen = false;
    close(searchPipe[1]);
    while(msg.command != 'Q')
    {
        read(searchPipe[0], (char *) &msg, sizeof(Message));
        cout << "Search server received message #" << msg.id << ", key = " << msg.key << ", payload = " << msg.payload << endl;
        if (msg.command == 'Q')
            exit(0);


        searchStream.open(msg.key, ios::in);
        if (!searchStream)  //otherwise we couldn't find/open the file, return 0.
        {
            fileOpen = false;
            write(returnPipe[1], notFoundString.c_str(), sizeof(notFoundString));

        }
        else    //if the file opens, it must be present, return 1.
        {
            fileOpen = true;
            getline(searchStream, payload);
            write(returnPipe[1], payload.c_str(), sizeof(payload));
        }
    }
}



/*
 * openPipes(int[], int[], int[], int[], int[])
 * Purpose: Opens all pipes used in the program.
 */
int openPipes(int putPipe[], int loggerPipe[], int numberPipe[], int searchPipe[], int returnPipe[] )
{
    int returnInt = 0;  //This will tell us if the pipe opening failed or not.

    //Open all the pipes--if there is an error--report it.

    if(pipe(putPipe) == -1)
    {
        returnInt = -1;
    }
    if(pipe(loggerPipe) == -1)
    {
        returnInt = -1;
    }
    if(pipe(numberPipe) == -1)
    {
        returnInt = -1;
    }
    if(pipe(searchPipe) == -1)
    {
        returnInt = -1;
    }
    if(pipe(returnPipe) == -1)
    {
        returnInt = -1;
    }

    return returnInt;
}

